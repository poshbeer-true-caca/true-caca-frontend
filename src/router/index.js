import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Media from '../views/Media.vue'
import InspireHub from '../views/InspireHub.vue'
import Photostock from '../views/Photostock.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/media',
    name: 'media',
    component: Media
  },
  {
    path: '/photostock',
    name: 'photostock',
    component: Photostock
  },
  {
    path: '/inspirehub',
    name: 'inspirehub',
    component: InspireHub
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
