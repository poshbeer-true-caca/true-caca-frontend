import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isOpenResearchPopup: false
  },
  mutations: {
    openResearchPopup (state) {
      state.isOpenResearchPopup = true
    },
    closeResearchPopup (state) {
      state.isOpenResearchPopup = false
    }
  },
  actions: {
    openResearchPopup ({ commit }) {
      commit('openResearchPopup')
    },
    closeResearchPopup ({ commit }) {
      commit('closeResearchPopup')
    }
  },
  getters: {
    isOpenResearchPopup: state => {
      return state.isOpenResearchPopup
    }
  },
  modules: {
  }
})
